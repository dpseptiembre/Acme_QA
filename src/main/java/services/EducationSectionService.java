/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.EducationSection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.EducationSectionRepository;

import java.util.Collection;

/**
 * Created by daviddelatorre on 12/3/17.
 */
@Service
@Transactional
public class EducationSectionService {

    // Constructors--------------------------------------------------------------------------------------

    @Autowired
    private EducationSectionRepository educationSectionRepository;

    // Managed repository--------------------------------------------------------------------------------

    public EducationSectionService() {
        super();
    }


    // Suporting services --------------------------------------------------------------------------------

    // Simple CRUD method --------------------------------------------------------------------------------

    public EducationSection create() {
        EducationSection res;
        res = new EducationSection();
        return res;
    }

    public Collection<EducationSection> findAll() {
        Collection<EducationSection> res;
        res = educationSectionRepository.findAll();
        Assert.notNull(res);
        return res;
    }

    public EducationSection findOne(int EducationSection) {
        domain.EducationSection res;
        res = educationSectionRepository.findOne(EducationSection);
        Assert.notNull(res);
        return res;
    }

    public EducationSection save(EducationSection a) {
        Assert.notNull(a);
        EducationSection res;
        res = educationSectionRepository.save(a);
        return res;
    }

    public void delete(EducationSection a) {
        Assert.notNull(a);
        Assert.isTrue(a.getId() != 0);
        educationSectionRepository.delete(a);
    }

    // Other business methods -------------------------------------------------------------------------------
    public void flush() {
        educationSectionRepository.flush();
    }
}
