/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.ProfesionalSection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.ProfesionalSectionRepository;

import java.util.Collection;

/**
 * Created by daviddelatorre on 12/3/17.
 */
@Service
@Transactional
public class ProfesionalSectionService {

    // Constructors--------------------------------------------------------------------------------------

    @Autowired
    private ProfesionalSectionRepository profesionalSectionRepository;

    // Managed repository--------------------------------------------------------------------------------

    public ProfesionalSectionService() {
        super();
    }


    // Suporting services --------------------------------------------------------------------------------

    // Simple CRUD method --------------------------------------------------------------------------------

    public ProfesionalSection create() {
        ProfesionalSection res;
        res = new ProfesionalSection();
        return res;
    }

    public Collection<ProfesionalSection> findAll() {
        Collection<ProfesionalSection> res;
        res = profesionalSectionRepository.findAll();
        Assert.notNull(res);
        return res;
    }

    public ProfesionalSection findOne(int ProfesionalSection) {
        domain.ProfesionalSection res;
        res = profesionalSectionRepository.findOne(ProfesionalSection);
        Assert.notNull(res);
        return res;
    }

    public ProfesionalSection save(ProfesionalSection a) {
        Assert.notNull(a);
        ProfesionalSection res;
        res = profesionalSectionRepository.save(a);
        return res;
    }

    public void delete(ProfesionalSection a) {
        Assert.notNull(a);
        Assert.isTrue(a.getId() != 0);
        profesionalSectionRepository.delete(a);
    }

    // Other business methods -------------------------------------------------------------------------------
    public void flush() {
        profesionalSectionRepository.flush();
    }
}
