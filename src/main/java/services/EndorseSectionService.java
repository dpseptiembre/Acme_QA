/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.EndorseSection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.EndorseSectionRepository;

import java.util.Collection;

/**
 * Created by daviddelatorre on 12/3/17.
 */
@Service
@Transactional
public class EndorseSectionService {

    // Constructors--------------------------------------------------------------------------------------

    @Autowired
    private EndorseSectionRepository endorseSectionRepository;

    // Managed repository--------------------------------------------------------------------------------

    public EndorseSectionService() {
        super();
    }


    // Suporting services --------------------------------------------------------------------------------

    // Simple CRUD method --------------------------------------------------------------------------------

    public EndorseSection create() {
        EndorseSection res;
        res = new EndorseSection();
        return res;
    }

    public Collection<EndorseSection> findAll() {
        Collection<EndorseSection> res;
        res = endorseSectionRepository.findAll();
        Assert.notNull(res);
        return res;
    }

    public EndorseSection findOne(int EndorseSection) {
        domain.EndorseSection res;
        res = endorseSectionRepository.findOne(EndorseSection);
        Assert.notNull(res);
        return res;
    }

    public EndorseSection save(EndorseSection a) {
        Assert.notNull(a);
        EndorseSection res;
        res = endorseSectionRepository.save(a);
        return res;
    }

    public void delete(EndorseSection a) {
        Assert.notNull(a);
        Assert.isTrue(a.getId() != 0);
        endorseSectionRepository.delete(a);
    }

    // Other business methods -------------------------------------------------------------------------------
    public void flush() {
        endorseSectionRepository.flush();
    }
}
