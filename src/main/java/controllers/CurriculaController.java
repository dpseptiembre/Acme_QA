/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.*;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/curricula")
public class CurriculaController extends AbstractController {

    //Services ----------------------------------------------------------------

    @Autowired
    private CurriculaService curriculaService;
    @Autowired
    private EducationSectionService educationSectionService;
    @Autowired
    private ProfesionalSectionService profesionalSectionService;
    @Autowired
    private EndorseSectionService endorseSectionService;
    @Autowired
    private TeacherService teacherService;


    //Constructors----------------------------------------------

    public CurriculaController() {
        super();
    }

    protected static ModelAndView createEditModelAndView(Curricula curricula) {
        ModelAndView result;

        result = createEditModelAndView(curricula, null);

        return result;
    }


    protected static ModelAndView createEditModelAndView(Curricula curricula, String message) {
        ModelAndView result;

        result = new ModelAndView("curricula/edit");
        result.addObject("curricula", curricula);
        result.addObject("message", message);

        return result;

    }






//EDUCATION RECORDS

    @RequestMapping(value = "/createEducationRecord", method = RequestMethod.GET)
    public ModelAndView createEducationRecord(@RequestParam int curriculaId) {

        ModelAndView result;

        EducationSection educationSection = educationSectionService.create();
        educationSection.setCurricula(curriculaService.findOne(curriculaId));
        result = createEditModelAndViewEducationRecord(educationSection);

        return result;

    }

    protected static ModelAndView createEditModelAndViewEducationRecord(EducationSection educationSection) {
        ModelAndView result;

        result = createEditModelAndViewEducationRecord(educationSection, null);

        return result;
    }


    protected static ModelAndView createEditModelAndViewEducationRecord(EducationSection educationSection, String message) {
        ModelAndView result;

        result = new ModelAndView("curricula/editEducationRecord");
        result.addObject("educationSection", educationSection);
        result.addObject("message", message);

        return result;

    }


    @RequestMapping(value = "/editEducationSectionSave", method = RequestMethod.POST, params = "save")
    public ModelAndView saveEducationRecord(@Valid EducationSection educationSection, BindingResult binding) {
        ModelAndView result;
        if (binding.hasErrors()) {
            result = createEditModelAndViewEducationRecord(educationSection);
        } else {
            try {
                educationSectionService.save(educationSection);
                result = new ModelAndView("user/success");
            } catch (Throwable oops) {
                result = createEditModelAndViewEducationRecord(educationSection, "curricula.commit.error");
            }
        }
        return result;
    }





//    PROFESIONAL RECORS

    @RequestMapping(value = "/createProfesionalRecord", method = RequestMethod.GET)
    public ModelAndView createProfesionalRecord(@RequestParam int curriculaId) {

        ModelAndView result;

        ProfesionalSection profesionalSection = profesionalSectionService.create();
        profesionalSection.setCurricula(curriculaService.findOne(curriculaId));

        result = createEditModelAndViewProfesional(profesionalSection);

        return result;

    }

    protected static ModelAndView createEditModelAndViewProfesional(ProfesionalSection profesionalSection) {
        ModelAndView result;

        result = createEditModelAndViewProfesional(profesionalSection, null);

        return result;
    }


    protected static ModelAndView createEditModelAndViewProfesional(ProfesionalSection profesionalSection, String message) {
        ModelAndView result;

        result = new ModelAndView("curricula/editProfessionalRecord");
        result.addObject("profesionalSection", profesionalSection);
        result.addObject("message", message);

        return result;

    }

    @RequestMapping(value = "/editProfesionalSectionSave", method = RequestMethod.POST, params = "save")
    public ModelAndView saveProfesionalRecords(@Valid ProfesionalSection profesionalSection, BindingResult binding) {
        ModelAndView result;
        if (binding.hasErrors()) {
            result = createEditModelAndViewProfesional(profesionalSection);
        } else {
            try {
                profesionalSectionService.save(profesionalSection);
                result = new ModelAndView("user/success");
            } catch (Throwable oops) {
                result = createEditModelAndViewProfesional(profesionalSection, "curricula.commit.error");
            }
        }
        return result;
    }









//    ENDORSER RECORDS

    @RequestMapping(value = "/createEndorseSection", method = RequestMethod.GET)
    public ModelAndView createEndorseSection(@RequestParam int curriculaId) {

        ModelAndView result;

        EndorseSection endorseSection = endorseSectionService.create();
        endorseSection.setCurricula(curriculaService.findOne(curriculaId));
        result = createEditModelAndViewEndorser(endorseSection);

        return result;

    }

    protected static ModelAndView createEditModelAndViewEndorser(EndorseSection endorseSection) {
        ModelAndView result;

        result = createEditModelAndViewEndorser(endorseSection, null);

        return result;
    }


    protected static ModelAndView createEditModelAndViewEndorser(EndorseSection endorseSection, String message) {
        ModelAndView result;

        result = new ModelAndView("curricula/editEndorserRecord");
        result.addObject("endorseSection", endorseSection);
        result.addObject("message", message);

        return result;

    }

    @RequestMapping(value = "/editEndorserSectionSave", method = RequestMethod.POST, params = "save")
    public ModelAndView saveEndorseRecord(@Valid EndorseSection endorseSection, BindingResult binding) {
        ModelAndView result;
        if (binding.hasErrors()) {
            result = createEditModelAndViewEndorser(endorseSection);
        } else {
            try {
                endorseSectionService.save(endorseSection);
                result = new ModelAndView("user/success");
            } catch (Throwable oops) {
                result = createEditModelAndViewEndorser(endorseSection, "curricula.commit.error");
            }
        }
        return result;
    }







    //Create Method -----------------------------------------------------------


    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView curriculasList() {

        ModelAndView result;
        Collection<Curricula> curricula;

        curricula = curriculaService.findAll();
        result = new ModelAndView("curricula/list");
        result.addObject("curriculas", curricula);
        result.addObject("requestURI", "curricula/list.do");

        return result;
    }


    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create() {

        ModelAndView result;

        Curricula curricula = curriculaService.create();
        result = createEditModelAndView(curricula);

        return result;

    }


    // Ancillary methods ------------------------------------------------


    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam int curriculaId) {
        ModelAndView result;
        Curricula curricula;

        curricula = curriculaService.findOne(curriculaId);
        Assert.notNull(curricula);
        Assert.isTrue(teacherService.findByPrincipal().getCurricula().equals(curricula));
        result = createEditModelAndView(curricula);

        return result;
    }





    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
    public ModelAndView save(@Valid Curricula curricula, BindingResult binding) {
        ModelAndView result;
        if (binding.hasErrors()) {
            result = createEditModelAndView(curricula);
        } else {
            try {
                curriculaService.save(curricula);
                result = new ModelAndView("redirect:list.do");
            } catch (Throwable oops) {
                result = createEditModelAndView(curricula, "curricula.commit.error");
            }
        }
        return result;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView delete2(@RequestParam int curriculaId) {
        ModelAndView result;
        try {
            Curricula curricula = curriculaService.findOne(curriculaId);
            Assert.isTrue(teacherService.findByPrincipal().getCurricula().equals(curricula));
            curriculaService.delete(curricula);
            result = new ModelAndView("redirect:list.do");
        } catch (Throwable oops) {
            Curricula curricula = curriculaService.findOne(curriculaId);
            result = createEditModelAndView(curricula, "curricula.commit.error");
        }

        return result;
    }

    @RequestMapping(value = "unapprobe", method = RequestMethod.GET)
    public ModelAndView ban(@RequestParam int curriculaId) {
        ModelAndView result;
        Boolean opq;
        Curricula curricula = curriculaService.findOne(curriculaId);
        opq = curriculaService.unapprobeCurricula(curricula);

        if (opq.equals(false)) {
            result = new ModelAndView("user/error");
        } else {
            result = new ModelAndView("redirect:list.do");
        }


        return result;
    }

    @RequestMapping(value = "approbe", method = RequestMethod.GET)
    public ModelAndView unban(@RequestParam int curriculaId) {
        ModelAndView result;
        Boolean op;
        Curricula curricula = curriculaService.findOne(curriculaId);
        op = curriculaService.approbeCurricula(curricula);

        if (op.equals(false)) {
            result = new ModelAndView("user/error");
        } else {
            result = new ModelAndView("redirect:list.do");
        }


        return result;
    }

}
