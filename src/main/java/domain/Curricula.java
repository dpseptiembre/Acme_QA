/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by daviddelatorre on 15/5/17.
 */
@Entity
@Access(AccessType.PROPERTY)
public class Curricula extends DomainEntity {

    private String photo;
    private boolean isApprobed;
    private Teacher owner;
    private PersonalSection personalSection;
    private Collection<EducationSection> educationSections;
    private Collection<EndorseSection> endorseSections;
    private Collection<ProfesionalSection> profesionalSections;


    @NotBlank
    @URL
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }


    public boolean isApprobed() {
        return isApprobed;
    }

    public void setApprobed(boolean approbed) {
        isApprobed = approbed;
    }

    @OneToOne(cascade = CascadeType.ALL)
    public Teacher getOwner() {
        return owner;
    }

    public void setOwner(Teacher owner) {
        this.owner = owner;
    }

    @OneToOne
    public PersonalSection getPersonalSection() {
        return personalSection;
    }

    public void setPersonalSection(PersonalSection personalSection) {
        this.personalSection = personalSection;
    }

    @OneToMany(mappedBy = "curricula")
    public Collection<EducationSection> getEducationSections() {
        return educationSections;
    }

    public void setEducationSections(Collection<EducationSection> educationSections) {
        this.educationSections = educationSections;
    }

    @OneToMany(mappedBy = "curricula")
    public Collection<EndorseSection> getEndorseSections() {
        return endorseSections;
    }

    public void setEndorseSections(Collection<EndorseSection> endorseSections) {
        this.endorseSections = endorseSections;
    }

    @OneToMany(mappedBy = "curricula")
    public Collection<ProfesionalSection> getProfesionalSections() {
        return profesionalSections;
    }

    public void setProfesionalSections(Collection<ProfesionalSection> profesionalSections) {
        this.profesionalSections = profesionalSections;
    }
}
