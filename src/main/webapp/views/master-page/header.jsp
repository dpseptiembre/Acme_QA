 <%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<div>
    <a href="welcome/index.do"> <img class="img-responsive" src="images/logo.png" alt="Acme Q&A, Inc."/>
    </a>
    <br>
</div>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <%--<div class="navbar-header">--%>
        <%--<a class="navbar-brand" href="#">Brand</a>--%>
        <%--</div>--%>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

                <security:authorize access="isAnonymous()">
                    <li><a href="user/create.do"><spring:message code="master.page.register.user"/></a></li>
                    <li><a href="teacher/create.do"><spring:message code="master.page.register.teacher"/></a></li>
                    <li><a href="question/listPopular.do"><spring:message code="master.page.question.listpop"/></a></li>
                    <li><a href="webinar/listIncoming.do"><spring:message code="master.page.webinar.listIncom"/></a></li>
                    <li><a href="webinar/listAn.do"><spring:message code="master.page.webinar.list"/></a></li>
                    <li><a href="question/listCat.do"><spring:message code="master.page.list.categories" /></a></li>
                    <li><a href="security/login.do"><spring:message code="master.page.login" /></a></li>

                </security:authorize>

                <%--****************************************ADMIN****************************************--%>

                <security:authorize access="hasRole('ADMIN')">
                    <li><a href="category/list.do"><spring:message code="master.page.categories"/> </a></li>
                    <li><a href="search/editCache.do"><spring:message code="master.page.changecache"/> </a></li>
                    <li><a href="administrator/editDuty.do"><spring:message code="master.page.changeDuty"/> </a></li>
                    <li><a href="admin/dashboard.do"><spring:message code="master.page.dashboard"/> </a></li>
                    <li><a href="curricula/list.do"><spring:message code="master.curriculas.list"/> </a></li>
                    <li><a href="question/listCat.do"><spring:message code="master.page.list.categories" /></a></li>
                    <li><a href="question/listPopular.do"><spring:message code="master.page.question.listpop"/></a></li>
                    <li><a href="user/list.do"><spring:message code="master.page.user.list"/> </a></li>

                </security:authorize>

                <%--****************************************USER****************************************--%>


                <security:authorize access="hasRole('USER')">
                <li><a href="user/editp.do"><spring:message code="master.page.edit.profile"/> </a></li>
                <li><a href="user/list.do"><spring:message code="master.page.user.list"/> </a></li>
                <li><a href="question/list.do"><spring:message code="master.page.question.list"/> </a></li>
                <li><a href="webinar/listAn.do"><spring:message code="master.page.webinar.list"/></a>
                <li><a href="webinar/listToGo.do"><spring:message code="master.page.webinar.mylist"/></a>
                <li><a href="bill/list.do"><spring:message code="master.page.webinar.mybill"/></a>
                    </security:authorize>

                    <%--****************************************TEACHER****************************************--%>

                    <security:authorize access="hasRole('TEACHER')">
                <li><a href="teacher/editp.do"><spring:message code="master.page.edit.profile"/> </a></li>
                <li><a href="teacher/viewCurricula.do"><spring:message code="master.page.view.curricula"/> </a></li>
                <li><a href="webinar/list.do"><spring:message code="master.page.webinar.list"/></a>
                <li><a href="webinar/listMy.do"><spring:message code="master.page.webinar.list.my"/></a>
                <li><a href="admin/dashboardT.do"><spring:message code="master.page.dashboard"/> </a></li>
                <li><a href="teacher/bill.do"><spring:message code="master.page.earns"/> </a></li>
                </security:authorize>

                <%--****************************************MODERATOR****************************************--%>

                <security:authorize access="hasRole('MODERATOR')">
                    <li><a href="user/listAll.do"><spring:message code="master.page.user.list"/></a></li>
                    <li><a href="question/listAll.do"><spring:message code="master.page.question.list"/> </a></li>
                </security:authorize>


                <security:authorize access="hasAnyRole('TEACHER','ADMIN','MODERATOR','USER')">
                    <li><a href="search/create.do"><spring:message code="master.page.search.new"/> </a></li>
                    <li><a href="search/mySearches.do"><spring:message code="master.page.search.last"/> </a></li>
                    <li><a href="/folder/list.do"><spring:message code="master.page.actor.mezzages"/></a></li>
                </security:authorize>

            </ul>

            <ul class="nav navbar-nav navbar-right">
                <security:authorize access="isAuthenticated()">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><spring:message code="master.page.profile" />(<security:authentication property="principal.username" />) <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="j_spring_security_logout"><spring:message code="master.page.profile.logout"/></a></li>
                        </ul>
                    </li>
                </security:authorize>

            </ul>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container">
    <a href="?language=en">EN</a> | <a href="?language=es">ES</a>
</div>

