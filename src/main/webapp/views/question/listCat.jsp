<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>
<div class="container">


    <c:forEach items="${cats}" var="cat">
        <h1>${cat.name}</h1>

<spring:message code="general.questions" var="register1111"/>
<h3><jstl:out value="${register1111}"/></h3>
<!-- Listing grid -->
<display:table pagesize="100" class="displaytag" keepStatus="true"
               name="${cat.questions}" requestURI="${requestURI}" id="row">


    <security:authorize access="permitAll()">
        <display:column>
            <a href="question/viewAn.do?questionId=${row.id}"> <spring:message
                    code="question.view"/>
            </a>
        </display:column>
    </security:authorize>

    <!-- Attributes -->
    <security:authorize access="isAnonymous()">
        <display:column>
            <a href="question/viewAn.do?questionId=${row.id}"> <spring:message
                    code="question.view"/>
            </a>
        </display:column>
    </security:authorize>

    <spring:message code="question.title" var="title"/>
    <display:column property="title" title="${title}" sortable="true"/>
    <spring:message code="question.summary" var="description"/>
    <display:column property="summary" title="${description}" sortable="true"/>
    <spring:message code="question.createdDate" var="originAddress"/>
    <display:column property="createdDate" title="${originAddress}" sortable="true"/>


</display:table>

    </c:forEach>
</div>