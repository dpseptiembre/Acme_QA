<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>


<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<form:form action="curricula/editProfesionalSectionSave.do" modelAttribute="profesionalSection">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="curricula"/>
    <form:hidden path="hidden"/>

    <acme:textbox path="companyName" code="profesional.companyName"/>
    <br/>
    <acme:textbox path="rol" code="profesional.rol"/>
    <br/>
    <acme:textbox path="attachment" code="profesional.attachment"/>
    <br/>
    <acme:textbox path="startDate" code="profesional.startDate"/>
    <br/>
    <acme:textbox path="endDate" code="profesional.endDate"/>
    <br/>

    <acme:submit name="save" code="general.save"/>

    <acme:cancel url="curricula/list.do" code="general.cancel"/>

</form:form>