<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>




<%--<jstl:out value="${cu.isApprobed}"/>--%>

<spring:message code="curricula.personaSection" var="personaSectione"/>
<h3><jstl:out value="${personaSectione}"/></h3>

<!-- Listing grid -->
<spring:message code="curricula.name" var="name1"/>
<h3><jstl:out value="${name1}"/></h3>
<jstl:out value="${cu.owner.name}"/>

<spring:message code="curricula.personaSection" var="personaSection1"/>
<h3><jstl:out value="${personaSection1}"/></h3>
<jstl:out value="${cu.owner.surname}"/>

<img src="<jstl:out value="${cu.photo}"/>"/>

<spring:message code="general.create" var="createg"/>

<spring:message code="curricula.educationSections" var="personaSection1"/>
<h3><jstl:out value="${personaSection1}"/></h3>

<a href="curricula/createEducationRecord.do?curriculaId=${cu.id}" class="button2"><jstl:out value="${createg}"/></a>
<br>

<c:forEach var ="value" items = "${cu.educationSections}">

    <jstl:out value="${value.title}"/>
    <jstl:out value="${value.expeditionInstitution}"/>
    <jstl:out value="${value.attachment}"/>

    <br>
    <jstl:out value="${value.startDate}"/>
    <jstl:out value="${value.endDate}"/>

    <c:forEach var = "comment" items = "${value.comments}">
        <c:out value="${comment}"/>

    </c:forEach>
    <hr>
</c:forEach>

<spring:message code="curricula.endorserSection" var="endorserSection1"/>
<h3><jstl:out value="${endorserSection1}"/></h3>

<a href="curricula/createEndorseSection.do?curriculaId=${cu.id}" class="button2"><jstl:out value="${createg}"/></a>
<br>


<c:forEach var ="value" items = "${cu.endorseSections}">

    <jstl:out value="${value.name}"/>
    <jstl:out value="${value.email}"/>
    <jstl:out value="${value.link}"/>


    <c:forEach var = "comment" items = "${value.comments}">
        <c:out value="${comment}"/>
    </c:forEach>
<hr>
</c:forEach>

<spring:message code="curricula.profesionalSections" var="profesionalSections1"/>
<h3><jstl:out value="${profesionalSections1}"/></h3>

<a href="curricula/createProfesionalRecord.do?curriculaId=${cu.id}" class="button2"><jstl:out value="${createg}"/></a>
<br>

<c:forEach var ="value" items = "${cu.profesionalSections}">

    <jstl:out value="${value.companyName}"/>
    <jstl:out value="${value.rol}"/>
    <jstl:out value="${value.attachment}"/>

    <jstl:out value="${value.startDate}"/>
    <jstl:out value="${value.endDate}"/>

    <hr>
</c:forEach>